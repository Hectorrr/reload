/*
** fondation.c for fondation in /Users/amorimgregory/Documents/projet_fondation
** 
** Made by Amorim Gregory
** Login   <amorim_g@etna-alternance.net>
** 
** Started on  Fri Feb  6 16:39:16 2015 Amorim Gregory
** Last update Fri Feb  6 16:39:27 2015 Amorim Gregory
*/

#include "liste.h"

void    display_list(t_liste *liste)
{
    t_liste *tmp;
    tmp = liste;
    my_putstr("\n");
    my_putstr("                           \033[31mNEXT\033[0m\n");
    my_putstr("                            |\n");
    my_putstr("                            |\n");
    while (tmp != NULL)
    {
      my_putstr("|------------------|        |\n");
      my_putstr("|       ");
      my_putstr(tmp->data);
      my_putstr("       |--------|\n");
      my_putstr("|------------------|        |\n");
      my_putstr("                            |\n");
      my_putstr("                            |\n");
      tmp = tmp->suivant;
    }
    my_putstr("                           \033[31mNULL\033[0m\n\n");

}

char search(t_liste *liste, char *str)
{
    t_liste *tmp;
    tmp = liste;
    char *t;
    int n;
    n = 1;
    my_putstr("SEARCH : ");
    my_putstr(str);
    my_putstr("\n");
    while (tmp != NULL)
    { 
      t = tmp->data;
      if (my_strcmp(t, str) == 0)
      {
        my_putstr("Élèment " );
        my_putstr(str);
        my_putstr(" : ");
        my_putstr("\x1B[5m");
        my_put_nbr(n);
        my_putstr("eme maillon \n");
        my_putstr("\033[0m");
        return (0);
      }
      else
        my_putstr("\033[31mNot found.\033[0m\n");
      n++;
      tmp = tmp->suivant;
    }
 return (0);
}

void    count_element(t_liste *liste)
{
   t_liste *tmp;
    tmp = liste;

    while (tmp != NULL)
    {
      my_put_nbr(my_strlen(tmp->data));
      tmp = tmp->suivant;
    }
}


t_liste    *reverse(t_liste *list)
{
  t_liste *inverse;
  t_liste *suivant;

  inverse = NULL;
  while (list != NULL)
  {
    suivant = list->suivant;
    list->suivant = inverse;
    inverse = list;
    list = suivant;
  }
  return (inverse);
}

t_liste    *add_list(t_liste *l, char *str)
{
    t_liste *new_liste;
    new_liste = malloc(sizeof(t_liste));
    new_liste->data = my_strdup(str);
    new_liste->suivant = l;
    return new_liste;
}

t_liste    *delete_list(t_liste *liste)
{
    t_liste *tmp;
    while (liste != NULL)
    {
      tmp =  liste->suivant;
      free(liste);
      liste = tmp;
    }
    return NULL;
}

int count(t_liste *liste)
{
        int n=0;
        while(liste)
          {
              n++;
              liste = liste->suivant;
          }
        return n;
}