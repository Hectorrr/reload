/*
** fondation.c for fondation in /Users/amorimgregory/Documents/projet_fondation
** 
** Made by Amorim Gregory
** Login   <amorim_g@etna-alternance.net>
** 
** Started on  Fri Feb  6 16:39:16 2015 Amorim Gregory
** Last update Fri Feb  6 16:39:27 2015 Amorim Gregory
*/

#include "liste.h"

int    main()
{



  /* 

  VERIFIER LE RETOUR DU MALLOC A LA CREATION DE LA LISTE 

  */
  int c;
  t_liste *list = NULL;
  
  list = reverse(add_list(list, "toto"));
  list = reverse(add_list(list, "tata"));
  list = reverse(add_list(list, "coco"));
  list = reverse(add_list(list, "tutu"));


  display_list(list);
  
  c = count(list);
  my_putstr("Votre liste contient ");
  my_put_nbr(c);
  my_putstr(" élèment(s).");
  my_putstr("\n\n");
  /*delete_list(list);
  for (i = 0;i < c; i++)
  {
    printf("toto");
  }*/
  //count_element(list);
  search(list, "toto");
  search(list, "greg");
  search(list, "coco");
  return (0);
}